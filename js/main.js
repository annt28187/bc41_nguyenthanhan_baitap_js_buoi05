// Bài 1.
const btnResultEl = document.getElementById("btnResult");

function areaStudent(value) {
  switch (value) {
    case "A":
      return 2;
    case "B":
      return 1;
    case "C":
      return 0.5;
    default:
      return 0;
  }
}

function subjectStudent(value) {
  switch (value) {
    case "1":
      return 2.5;
    case "2":
      return 1.5;
    case "3":
      return 1;
    default:
      return 0;
  }
}

btnResultEl.addEventListener("click", function () {
  var benchMask = document.getElementById("inputBenchMask").value * 1;
  var selLocationEl = document.getElementById("selLocation");
  var textSelLocation = selLocationEl.options[selLocationEl.selectedIndex].text;
  var selUserEl = document.getElementById("selUser");
  var textSelUserEl = selUserEl.options[selUserEl.selectedIndex].text;
  var fistSubMask = document.getElementById("inputScore1").value * 1;
  var secondSubMask = document.getElementById("inputScore2").value * 1;
  var thirdSubMask = document.getElementById("inputScore3").value * 1;
  var valueSelLocation = areaStudent(textSelLocation);
  var valueSelUserEl = subjectStudent(textSelUserEl);
  var totalMark =
    valueSelLocation +
    valueSelUserEl +
    fistSubMask +
    secondSubMask +
    thirdSubMask;

  if (fistSubMask == 0 || secondSubMask == 0 || thirdSubMask == 0) {
    document.getElementById(
      "txtResult"
    ).innerHTML = `Bạn đã rớt. Do có môn thi bị điểm 0`;
  } else if (totalMark < benchMask) {
    document.getElementById(
      "txtResult"
    ).innerHTML = `Bạn đã rớt. Tổng điểm: ${totalMark}`;
  } else {
    document.getElementById(
      "txtResult"
    ).innerHTML = `Bạn đã đậu. Tổng điểm: ${totalMark}`;
  }
});

// Bài 2
const btnElecBillEl = document.getElementById("btnElecBill");

btnElecBillEl.addEventListener("click", function () {
  var inputNameEl = document.getElementById("inputName").value;
  var inputKWEl = document.getElementById("inputKW").value * 1;
  var totalEctBill = 0;
  if (inputKWEl <= 50) {
    totalEctBill = inputKWEl * 500;
  } else if (inputKWEl <= 100) {
    totalEctBill = 50 * 500 + (inputKWEl - 50) * 650;
  } else if (inputKWEl <= 200) {
    totalEctBill = 50 * 500 + 50 * 650 + (inputKWEl - 100) * 850;
  } else if (inputKWEl <= 350) {
    totalEctBill = 50 * 500 + 50 * 650 + 100 * 850 + (inputKWEl - 200) * 1100;
  } else {
    totalEctBill =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (inputKWEl - 350) * 1300;
  }
  totalEctBill = Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  }).format(totalEctBill);
  document.getElementById(
    "txtElecBill"
  ).innerHTML = `Họ tên ${inputNameEl}. Tổng tiền điện: ${totalEctBill}`;
});
